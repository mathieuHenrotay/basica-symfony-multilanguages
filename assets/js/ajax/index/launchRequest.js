// ./assets/js/ajax/launchRequest.js

// script qui définit des actions sur la réponse du serveur
import requestResponse from './requestResponse';

export default function(RequeteAjax){
  //requête ajax avec l'objet de configuration
  $.ajax({
      url: RequeteAjax.url,
      method: RequeteAjax.method,
      data: RequeteAjax.data,
      success: function(reponseSrv){
        //script agissant sur la réponse du serveur
        requestResponse(reponseSrv, RequeteAjax);
        console.log(RequeteAjax);
      },
      error: function(){
        window.alert("Problème durant la transaction...");
      }
  });
}
