// ./assets/js/ajax/requestResponse.js
// script qui agit sur une réponse du serveur d'une requête ajax

export default function(reponseSrv, RequeteAjax) {
  // j'ajoute la réponse du serveur
  $(RequeteAjax.reponse.appendTo).append(reponseSrv);
  //je calcule le nombre d'éléments que je viens d'appender
  var nbreElem = parseInt($(RequeteAjax.reponse.appendTo+' >:last-child').attr('data-offset'));
  console.log(nbreElem);
  //Si j'en ai ajouté, je slide down les éléments et j'incrémente l'offset
  $(RequeteAjax.reponse.appendTo+' >:nth-last-child(-n+'+nbreElem+')').hide().slideDown();
  RequeteAjax.data.offset+=nbreElem;
  if (typeof(RequeteAjax.reponse.btn) !== 'undefined') {

    if ($(RequeteAjax.reponse.appendTo+' >:last-child').attr('data-more') == 'false') {
      $(RequeteAjax.reponse.btn).fadeOut(function(){
        $(this).remove();
      });
    }

    // j'enlève le focus du bouton
    $(':focus').blur();
  }

};
