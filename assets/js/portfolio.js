// ./assets/js/portfolio.js
// script qui affiche les works suivants le cas échéant

//script contenant la requête ajax
import launchRequest from './ajax/index/launchRequest';

$(function(){
  //je définis une variable offset car j'en aurais besoin à plussieurs reprise dans mon objet
  var offset = parseInt($('#lastsWorks div:last-of-type').attr('data-offset'));
  //je crée mon objet de configuration
  var RequeteAjax = {
    url: $('#moreWorks').attr('data-route'),
    data: {
      offset: offset,
      limit: offset,
      orderBy: {
        dateRealisation: 'DESC'
      }
    },
    method:'get',
    reponse: {
      appendTo: '#lastsWorks',
      btn: '#moreWorks'
    }
  };

  // Quand je clique
  $('#moreWorks a').click(function(e){
    // j'empêche le comportement naturel
    e.preventDefault();
    // je lance ma requête
    launchRequest(RequeteAjax);
  });

});
