/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/bootstrap.min.css');
require('../css/main.css');
require('../css/custom.css');
require('../css/icomoon-social.css');
require('../css/font-awesome.min.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
require('../js/jquery-1.9.1.min.js');
require('../js/bootstrap.min.js');
require('../js/jquery.easing.min.js');
require('../js/scrolling-nav.js');
