-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 29, 2019 at 03:59 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `basica-symfony-multilingue`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `categorie_translation`
--

CREATE TABLE `categorie_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie_translation`
--

INSERT INTO `categorie_translation` (`id`, `translatable_id`, `nom`, `slug`, `locale`) VALUES
(1, 1, 'Productivity', 'productivity', 'en'),
(2, 1, 'Productivité', 'productivite', 'fr'),
(3, 2, 'PHP', 'php', 'en'),
(4, 2, 'PHP', 'php', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `nom`) VALUES
(1, 'Apple');

-- --------------------------------------------------------

--
-- Table structure for table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_publication` datetime NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `createur`
--

CREATE TABLE `createur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `createur`
--

INSERT INTO `createur` (`id`, `nom`) VALUES
(1, 'Mathieu');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'admin', 'admin', 'mathieu.henrotay@gmail.com', 'mathieu.henrotay@gmail.com', 1, NULL, '$2y$13$Gp9anaPxh0U2GotgUAuEle4UoZ1JewW9/dzeHUcyDJweoisazodjC', '2019-04-29 14:55:56', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}');

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190408231752', '2019-04-08 23:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `tri` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `tri`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `page_translation`
--

CREATE TABLE `page_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_translation`
--

INSERT INTO `page_translation` (`id`, `translatable_id`, `nom_menu`, `nom`, `slug`, `texte`, `locale`) VALUES
(1, 1, 'Home', 'home', 'home', NULL, 'en'),
(2, 1, 'Accueil', 'accueil', 'accueil', NULL, 'fr'),
(3, 2, 'Portfolio', 'Our portfolio', 'portfolio', '<h2>We are leading company</h2>\r\n\r\n<h3>Specializing in Wordpress Theme Development</h3>\r\n\r\n<p>Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis.</p>', 'en'),
(4, 2, 'Portfolio', 'Notre portfolio', 'portfolio', '<h2>Nous sommes une une entreprise innovante</h2>\r\n\r\n<h3>Sp&eacute;cialis&eacute;e dans le d&eacute;veloppement de th&egrave;mes Wordpress</h3>\r\n\r\n<p>Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis.</p>', 'fr'),
(5, 3, 'Blog', 'Our blog', 'blog', NULL, 'en'),
(6, 3, 'Blog', 'Notre blog', 'blog', NULL, 'fr'),
(7, 4, 'Contact', 'Contact us', 'contact', NULL, 'en'),
(8, 4, 'Contact', 'Nous contacter', 'contact', NULL, 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `date_publication` datetime NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_modification` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `date_publication`, `image`, `date_modification`) VALUES
(4, '2019-04-10 20:02:16', '1.jpg', '2019-04-16 20:28:28'),
(5, '2019-04-15 08:05:04', '2.jpg', '2019-04-16 09:59:41'),
(6, '2019-04-15 14:38:01', '3.jpg', '2019-04-15 14:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `post_categorie`
--

CREATE TABLE `post_categorie` (
  `post_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_categorie`
--

INSERT INTO `post_categorie` (`post_id`, `categorie_id`) VALUES
(4, 1),
(4, 2),
(5, 1),
(6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_translation`
--

CREATE TABLE `post_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_translation`
--

INSERT INTO `post_translation` (`id`, `translatable_id`, `nom`, `slug`, `texte`, `locale`) VALUES
(5, 4, 'wonderful article', 'wonderful-article', '<h1>oulakajkfhsdjfsdkjf</h1>\r\n\r\n<p>fsjfsifjsklfjskfjisdofjoskdfjksf dsqdqsdsq sqsqdqsd&nbsp;</p>', 'en'),
(6, 4, 'Un super article de blog', 'un-super-article-de-blog', '<h1>oulala</h1>\r\n\r\n<p>waouw</p>', 'fr'),
(7, 5, 'another blog article', 'another-blog-article', '<h3>hello</h3>\r\n\r\n<p>that&#39;s so gud</p>', 'en'),
(8, 5, 'un autre article de blog', 'un-autre-article-de-blog', '<p>tellement de texte youpie</p>', 'fr'),
(9, 6, 'blog strikes again', 'blog-strikes-again', '<p>lolololololo</p>', 'en'),
(10, 6, 'toujours plus de blogs', 'toujours-plus-de-blogs', '<p>lalalalalalalal</p>', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`) VALUES
(1),
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tag_translation`
--

CREATE TABLE `tag_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_translation`
--

INSERT INTO `tag_translation` (`id`, `translatable_id`, `nom`, `slug`, `locale`) VALUES
(1, 1, 'Development', 'developmen', 'en'),
(2, 1, 'Développement', 'developpement', 'fr'),
(5, 3, 'Design', 'design', 'en'),
(6, 3, 'Design', 'design', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `createur_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_realisation` datetime NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mis_en_avant` tinyint(1) NOT NULL,
  `date_modification` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`id`, `createur_id`, `client_id`, `date_realisation`, `image`, `mis_en_avant`, `date_modification`) VALUES
(4, 1, 1, '2014-01-01 00:00:00', '1.jpg', 1, '2019-04-09 05:36:35'),
(5, 1, 1, '2019-01-15 00:00:00', '2.jpg', 1, '2019-04-25 12:06:05'),
(7, 1, 1, '2014-01-01 00:00:00', '6.jpg', 0, '2019-04-15 07:34:32'),
(8, 1, 1, '2019-04-05 00:00:00', '8.jpg', 0, '2019-04-23 21:28:52'),
(9, 1, 1, '2019-04-22 19:00:00', '5.jpg', 1, '2019-04-22 19:51:31'),
(10, 1, 1, '2019-04-15 12:00:00', '3.jpg', 1, '2019-04-28 22:18:14'),
(11, 1, 1, '2019-04-28 22:17:00', '4.jpg', 0, '2019-04-28 22:17:57'),
(12, 1, 1, '2019-04-29 15:27:00', '7.jpg', 0, '2019-04-29 15:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `work_tag`
--

CREATE TABLE `work_tag` (
  `work_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_tag`
--

INSERT INTO `work_tag` (`work_id`, `tag_id`) VALUES
(4, 1),
(5, 1),
(5, 3),
(7, 1),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_translation`
--

CREATE TABLE `work_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_translation`
--

INSERT INTO `work_translation` (`id`, `translatable_id`, `nom`, `slug`, `texte`, `locale`) VALUES
(6, 4, 'testEN', 'test-en', 'testEN', 'en'),
(7, 4, 'test', 'test', 'test', 'fr'),
(8, 5, 'test2 en', 'test2-en', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p>', 'en'),
(9, 5, 'test2 en route les amis', 'test2-en-route-les-amis', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p>', 'fr'),
(12, 7, 'toujours un joli test', 'toujours-un-joli-test', '<h1>voici un nouveau test</h1>\r\n\r\n<hr />\r\n<p>quel test de fou les amis!</p>\r\n\r\n<p><img alt=\"laugh\" height=\"23\" src=\"http://localhost/2eme/projets/basica/realisation/public/bundles/fosckeditor/plugins/smiley/images/teeth_smile.png\" title=\"laugh\" width=\"23\" /></p>\r\n\r\n<ul>\r\n	<li>olalala</li>\r\n	<li>ululu</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li>un</li>\r\n	<li>deux</li>\r\n	<li>trois</li>\r\n</ol>', 'fr'),
(13, 8, 'once again some tests', 'once-again-some-tests', '<p>blabliblu</p>', 'en'),
(14, 8, 'encore et toujours des tests', 'encore-et-toujours-des-tests', '<p>ouloulou</p>', 'fr'),
(15, 7, 'it\'s in english', 'its-in-english', '<p>that&#39;s good shit dude</p>', 'en'),
(16, 9, 'what a work !', 'what-a-work', '<p>so wondeful man !</p>', 'en'),
(17, 9, 'Un super work', 'un-super-work', '<p>trop cool et beau !</p>', 'fr'),
(18, 10, 'still works', 'still-works', '<p>waouw</p>', 'en'),
(19, 10, 'toujours des works', 'toujours-des-works', '<p>trop bien!</p>', 'fr'),
(20, 11, 'zeezezaeaz', 'zeezezaeaz', '<p>ezaaezaze zaezae zeazaazeze</p>', 'en'),
(21, 11, 'azeeazazezaze', 'azeeazazezaze', '<p>azezaaeeazeeza azeeazaezeza</p>', 'fr'),
(22, 12, 'so good I can\'t believe it', 'so-good-i-cant-believe-it', '<p>Damn son where did you find this ?!</p>', 'en'),
(23, 12, 'l\'petit dernier pour l\'route là', 'lpetit-dernier-pour-lroute-la', '<p>Didju ti c&#39;est beau !</p>', 'fr');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6B9763662C2AC5D3` (`translatable_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_67F068BC4B89032C` (`post_id`),
  ADD KEY `IDX_67F068BCA76ED395` (`user_id`);

--
-- Indexes for table `createur`
--
ALTER TABLE `createur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translation`
--
ALTER TABLE `page_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translation_unique_translation` (`translatable_id`,`locale`(180)),
  ADD KEY `IDX_A3D51B1D2C2AC5D3` (`translatable_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_categorie`
--
ALTER TABLE `post_categorie`
  ADD PRIMARY KEY (`post_id`,`categorie_id`),
  ADD KEY `IDX_E813A8C34B89032C` (`post_id`),
  ADD KEY `IDX_E813A8C3BCF5E72D` (`categorie_id`);

--
-- Indexes for table `post_translation`
--
ALTER TABLE `post_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_5829CF40989D9B62` (`slug`(180)),
  ADD UNIQUE KEY `post_translation_unique_translation` (`translatable_id`,`locale`(180)),
  ADD KEY `IDX_5829CF402C2AC5D3` (`translatable_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_translation`
--
ALTER TABLE `tag_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_translation_unique_translation` (`translatable_id`,`locale`(180)),
  ADD KEY `IDX_A8A03F8F2C2AC5D3` (`translatable_id`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_534E688073A201E5` (`createur_id`),
  ADD KEY `IDX_534E688019EB6921` (`client_id`);

--
-- Indexes for table `work_tag`
--
ALTER TABLE `work_tag`
  ADD PRIMARY KEY (`work_id`,`tag_id`),
  ADD KEY `IDX_79E7E01FBB3453DB` (`work_id`),
  ADD KEY `IDX_79E7E01FBAD26311` (`tag_id`);

--
-- Indexes for table `work_translation`
--
ALTER TABLE `work_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_ABA8A5AC989D9B62` (`slug`(180)),
  ADD UNIQUE KEY `work_translation_unique_translation` (`translatable_id`,`locale`(180)),
  ADD KEY `IDX_ABA8A5AC2C2AC5D3` (`translatable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `createur`
--
ALTER TABLE `createur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `page_translation`
--
ALTER TABLE `page_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `post_translation`
--
ALTER TABLE `post_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tag_translation`
--
ALTER TABLE `tag_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `work_translation`
--
ALTER TABLE `work_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  ADD CONSTRAINT `FK_6B9763662C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `FK_67F068BC4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_67F068BCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `page_translation`
--
ALTER TABLE `page_translation`
  ADD CONSTRAINT `FK_A3D51B1D2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_categorie`
--
ALTER TABLE `post_categorie`
  ADD CONSTRAINT `FK_E813A8C34B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E813A8C3BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_translation`
--
ALTER TABLE `post_translation`
  ADD CONSTRAINT `FK_5829CF402C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `post` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tag_translation`
--
ALTER TABLE `tag_translation`
  ADD CONSTRAINT `FK_A8A03F8F2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `FK_534E688019EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FK_534E688073A201E5` FOREIGN KEY (`createur_id`) REFERENCES `createur` (`id`);

--
-- Constraints for table `work_tag`
--
ALTER TABLE `work_tag`
  ADD CONSTRAINT `FK_79E7E01FBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_79E7E01FBB3453DB` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work_translation`
--
ALTER TABLE `work_translation`
  ADD CONSTRAINT `FK_ABA8A5AC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `work` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
