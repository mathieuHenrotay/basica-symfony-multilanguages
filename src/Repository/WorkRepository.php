<?php

namespace App\Repository;

use App\Entity\Work;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Work|null find($id, $lockMode = null, $lockVersion = null)
 * @method Work|null findOneBy(array $criteria, array $orderBy = null)
 * @method Work[]    findAll()
 * @method Work[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Work::class);
    }

    /**
     * permet de trouver les works similaire à un work particulier en les triant par nombre de tag commun
     * @param  Work    $work  work dont on cherche les works similaires
     * @param  integer $limit limitation du nombre de résultat
     * @return Work[]         ensemble des Work similaires
     */
    public function findAllRelatedToTags(Work $work, INT $limit=4){
      $tags= $work->getTags();
      $rsm = new ResultSetMappingBuilder($this->getEntityManager());
      $rsm->addRootEntityFromClassMetadata('App\Entity\Work', 'w');
      $sql = "SELECT w.*
              FROM work w
              JOIN work_tag wt ON w.id = wt.work_id
              WHERE ( 1=0";
      foreach ($tags as $index => $tag) {
        $sql.=" OR wt.tag_id = :tagId".$index;
      }
      $sql.=")";
      $sql.=" AND w.id NOT IN(:workId)
              GROUP BY w.id
              ORDER BY count(w.id) DESC, w.date_realisation DESC
              LIMIT :limite";
      $query = $this->_em->createNativeQuery($sql, $rsm);
      foreach ($tags as $index => $tag) {
        $query->setParameter('tagId'.$index, $tag->getId());
      }
      $query->setParameter('workId', $work->getId());
      $query->setParameter('limite', $limit);
      return $query->getResult();
    }

    /**
     * renvoies une liste de Work limitée d'un même tag
     * @param  array  $where   condition sur les résultats (limitation à un tag)
     * @param  array  $orderBy inutile! vient du controller générique
     * @param  int $limit  limitation du nombre de résultats
     * @param  int $offset  offset offset des résultats
     * @return Work[]  liste de Work limitée du tag
     */
    public function findByTag(array $where = [], array $orderBy = [], int $limit = null, int $offset = null){

      if (!empty($where)) {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('App\Entity\Work', 'w');

        $sql = 'SELECT w.*
                FROM work w
                JOIN work_tag wt ON wt.work_id = w.id
                WHERE wt.tag_id = :id
                ORDER BY w.date_realisation DESC';
        if ($limit) {
          $sql.= ' LIMIT :limit';
        }
        if ($offset) {
          $sql.= ' OFFSET :offset';
        }

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(':id', $where['tag']);
        if ($limit) {
          $query->setParameter(':limit', $limit);
        }
        if ($offset) {
          $query->setParameter(':offset', $offset);
        }

        return $query->getResult();
      }

    }

    // /**
    //  * @return Work[] Returns an array of Work objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Work
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
