<?php

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    /**
     * renvoies une liste de Tag d'un même Work
     * @param  array  $where   condition sur les résultats (limitation à un work et à la locale actuelle)
     * @param  array  $orderBy inutile! vient du controller générique
     * @param  int $limit  limitation du nombre de résultats
     * @param  int $offset  offset offset des résultats
     * @return Work[]  liste de Tag du Work
     */
    public function findByWork(array $where = [], array $orderBy = [], int $limit = null, int $offset = null){

      if (!empty($where)) {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('App\Entity\Tag', 't');

        $sql = 'SELECT t.*
                FROM tag t
                JOIN work_tag wt ON wt.tag_id = t.id
                JOIN tag_translation tt ON t.id = tt.translatable_id
                WHERE wt.work_id = :id
                  AND tt.locale = :locale
                ORDER BY tt.nom ASC';
        if ($limit) {
          $sql.= ' LIMIT :limit';
        }
        if ($offset) {
          $sql.= ' OFFSET :offset';
        }

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(':id', $where['work']);
        $query->setParameter(':locale', $where['locale']);
        if ($limit) {
          $query->setParameter(':limit', $limit);
        }
        if ($offset) {
          $query->setParameter(':offset', $offset);
        }

        return $query->getResult();
      }
    }
    // /**
    //  * @return Tag[] Returns an array of Tag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tag
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
