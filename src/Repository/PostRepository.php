<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * Permet de récuperer tous les posts d'une catégorie et de les trier par ordre de publication (on ne peut donc pas utiliser $posts dans Categorie car on ferait 2 fois le travail: les récuperer puis les trier)
     * @param  array   $where   id de la categorie
     * @param  array   $orderBy inutile ici
     * @param  int  $limit   limitation eventuel
     * @param  integer $offset  offset eventuel
     * @return Post[]           Post de la categorie
     */
    public function findAllByCategorie(array $where = [], array $orderBy = [], $limit = null, $offset = 0){
      $rsm = new ResultSetMappingBuilder($this->getEntityManager());
      $rsm->addRootEntityFromClassMetadata('App\Entity\Post', 'p');
      $sql = "SELECT p.*
              FROM post p
              JOIN post_categorie pc ON p.id = pc.post_id
              WHERE pc.categorie_id = :id
              ORDER BY p.date_publication DESC ";
      if ($limit) {
        $sql.= "LIMIT :limit ";
      }
      if ($offset != 0) {
        $sql.= "OFFSET :offset";
      }

      $query = $this->_em->createNativeQuery($sql, $rsm);
      $query->setParameter('id', $where['categorie']);
      if ($limit) {
        $query->setParameter('limit', $limit);
      }
      if ($offset != 0) {
        $query->setParameter('offset', $offset);
      }

      return $query->getResult();
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
