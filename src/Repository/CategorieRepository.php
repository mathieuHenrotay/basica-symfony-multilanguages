<?php

namespace App\Repository;

use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Categorie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categorie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categorie[]    findAll()
 * @method Categorie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Categorie::class);
    }

    /**
     * Permet de récuperer la liste des categories par ordre alphabétique selon la langue (utile dans l'indexAction car le nom de la catégorie est dans la table de traduction)
     * @param  string $locale langue de l'utilisateur
     * @return Categorie[]         tableau contenant toutes les categories ordonnées
     */
    public function findAllOrderByNom(string $locale){
      $rsm = new ResultSetMappingBuilder($this->getEntityManager());
      $rsm->addRootEntityFromClassMetadata('App\Entity\Categorie', 'c');
      $sql = "SELECT c.*
              FROM categorie c
              JOIN categorie_translation ct ON c.id = ct.translatable_id
              WHERE ct.locale = :locale
              ORDER BY ct.nom ASC";
      $query = $this->_em->createNativeQuery($sql, $rsm);
      $query->setParameter('locale', $locale);
      return $query->getResult();
    }

    // /**
    //  * @return Categorie[] Returns an array of Categorie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Categorie
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
