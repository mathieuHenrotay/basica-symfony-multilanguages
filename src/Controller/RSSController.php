<?php
/*
    ./vendor/ieps/Core/GenericController.php
 */
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;
/**
 * Controller des flux RSS
 */
class RSSController extends AbstractController {
  /**
   * renvoies une vue avec un flux rss dedans
   * @param string $url url valide vers un flux rss
   * @param string $vue nom de la vue à renvoyer
   * @return Symfony\Component\HttpFoundation\Response  vue avec le flux à l'intérieur
   */
  public function RSSAction(string $url = null, string $vue = 'feed'){
    $rss = simplexml_load_file($url);

    return $this->render('rss/'.$vue.'.html.twig', [
            'rss' => $rss,
    ]);
  }
}
