<?php
/*
  ./src/Controller/WorkController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Work;
use Symfony\Component\HttpFoundation\Request;
/**
 * controleur des Work
 */
class WorkController extends GenericController {
  /**
   * renvoie une vue avec tous les Work qui ont le même tag qu'un Work prédéfini. De plus ils sont ordonnés par nombre de tags commun avec le dit Work
   * @param  Work   $work Work sur base du quel on recherche les autres Work
   * @return Symfony\Component\HttpFoundation\Response  vue avec les Work liés dedans
   */
  public function relatedAction(Work $work){
    $works = $this->_repository->findAllRelatedToTags($work);
    return $this->render('works/related.html.twig',[
      'works' => $works
    ]);
  }

}
