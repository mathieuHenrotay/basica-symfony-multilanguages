<?php
namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Controller qui extends le controleur par défaut du plugin easyAdmin
 */
class AdminController extends EasyAdminController
{
  /**
   * @Route(
   *     "/dashboard",name="easyadmin"
   * )
   */
  public function indexAction(Request $request)
  {
    return parent::indexAction($request);
  }

  public function persistEntity($entity)
  {
      $this->updateDatePublication($entity);
      $this->updateDateModification($entity);
      parent::persistEntity($entity);
  }

  public function updateEntity($entity)
  {
      $this->updateDateModification($entity);
      parent::updateEntity($entity);
  }

  private function updateDateModification($entity)
  {
      if (method_exists($entity, 'setDateModification')) {
          $entity->setDateModification(new \DateTime());
      }
  }

  private function updateDatePublication($entity)
  {
      if (method_exists($entity, 'setDatePublication')) {
          $entity->setDatePublication(new \DateTime());
      }
  }

}
