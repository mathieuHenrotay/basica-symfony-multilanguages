<?php
/*
  ./src/Controller/PageController.php
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;

/**
 * Le controller des créatures
 */
class PageController extends GenericController {

  /**
   * Renvoies une vue avec l'index des pages
   * j'ai choisi de ne pas utiliser l'indexAction car sinon mon indexAction générique devait être compatible avec celui-ci (verification inutile sur la route actuelle pour tous les indexAction de mes autres entités -> pas optimisé)
   * @param  array  $activeParams paramètre de la route actuelle
   * @param  string  $activeRoute  nom de la route actuelle
   * @param  string  $vue          vue à renvoyer
   * @param  array   $where        condition éventuelle ['champ'=>'valeur']
   * @param  array   $orderBy      ordination éventuelle ['champ'=>'sens']
   * @param  int  $limit        limitation éventuelle
   * @param  Request $request      paramètre de la requête actuelle
   * @return Symfony\Component\HttpFoundation\Response  vue avec l'ensemble des Page disponibles
   */
  public function menuAction(array $activeParams = null, string $activeRoute, string $vue = 'menu', $where = [], array $orderBy = ['tri' => 'ASC'], int $limit = null, Request $request){
    $pages = $this->_repository->findBy($where, $orderBy, $limit);

    if( $activeRoute == 'app.pages.show' || $activeRoute == 'app.homepage' ):
      $activeId = $activeParams['id'];
    else:
      $activeId=null;
    endif;
    return $this->render('pages/'.$vue.'.html.twig',[
      'pages' => $pages,
      'activeId' => $activeId
    ]);
  }

}
