<?php
/*
  ./src/Controller/CategorieController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request;
/**
 * controleur des Categorie
 */
class CategorieController extends GenericController {

  /**
   * Surcharge de l'indexAction afin de pouvoir trier par nom (présent dans la table de traduction). renvoies une vue avec l'index de l'entité
   * @param  string  $vue     vue à renvoyer
   * @param  array   $where   Inutile! présent uniquement pour surcharger
   * @param  array   $orderBy Inutile! présent uniquement pour surcharger
   * @param  int  $limit   Inutile! présent uniquement pour surcharger
   * @param  int  $offset  Inutile! présent uniquement pour surcharger
   * @param  Request $request requête actuelle
   * @return Symfony\Component\HttpFoundation\Response  vue avec l'index de l'entité dedans
   */
  public function indexAction(string $vue = 'index', string $methode = 'findBy', array $where = [], array $orderBy = [], int $limit = null, int $offset = null, Request $request){
    $locale = $request->getLocale();
    $categories = $this->_repository->findAllOrderByNom($locale);
    return $this->render('categories/'.$vue.'.html.twig',[
      'categories' => $categories
    ]);
  }

}
