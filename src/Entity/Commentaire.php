<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * date de publication de l'entité
     * @var \DateTime
     */
    private $datePublication;

    /**
     * @ORM\Column(type="text")
     * texte de l'entité
     * @var string
     */
    private $texte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     * Post de l'entité
     * @var Post
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * User de l'entité
     * @var User
     */
    private $user;

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * recupère la date de publication de l'entité
     * @return \DateTime date de publication
     */
    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->datePublication;
    }

    /**
     * définit la date de publication de l'entité
     * @return self objet en cours
     */
    public function setDatePublication(\DateTimeInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * recupère le texte de l'entité
     * @return string texte de l'entité
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * définit le texte de l'entité
     * @return self objet en cours
     */
    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * récupère le post de l'entité
     * @return Post Post de l'entité
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * définit le Post de l'entité
     * @return self objet en cours
     */
    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * récupère le User de l'entité
     * @return User User de l'entité
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * définit le User de l'entité
     * @return self objet en cours
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getTexte();
    }
}
