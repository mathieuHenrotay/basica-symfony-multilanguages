<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class WorkTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=255)
     * nom de l'entité
     * @var string
     */
    private $nom;

    /**
     * @Gedmo\Slug(fields={"nom"}, unique=false)
     * @ORM\Column(type="string", length=255)
     * slug de l'entité
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     * texte de l'entité
     * @var string
     */
    private $texte;

    /**
     * recupère le nom de l'entité
     * @return string nom de l'entité
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * définit le nom de l'entité
     * @return self objet en cours
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * recupère le texte de l'entité
     * @return string texte de l'entité
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }

    /**
     * définit le texte de l'entité
     * @return self objet en cours
     */
    public function setTexte(?string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * recupère le slug de l'entité
     * @return string slug de l'entité
     */
    public function getSlug()
    {
        return $this->slug;
    }

}
