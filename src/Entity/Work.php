<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkRepository")
 * @Vich\Uploadable
 */
class Work
{
    use ORMBehaviors\Translatable\Translatable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * date de realisation de l'entité
     * @var \DateTime
     */
    private $dateRealisation;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="work_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="boolean")
     * définit si un post est en avant ou pas
     * @var bool
     */
    private $misEnAvant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Createur", inversedBy="works")
     * @ORM\JoinColumn(nullable=false)
     * Createur de l'entité
     * @var Createur
     */
    private $createur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="works")
     * @ORM\JoinColumn(nullable=false)
     * Createur de l'entité
     * @var Client
     */
    private $client;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="works")
     * ensemble des Tag de l'entité
     * @var Tag[]
     */
    private $tags;

    /**
     * @ORM\Column(type="datetime")
     * date de modification de l'entité
     * @var \DateTime
     */
    private $dateModification;

    /**
     * contructeur d'objet
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->setDateRealisation(new \DateTime());
    }

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * recupère la date de realisation de l'entité
     * @return \DateTime date de realisation
     */
    public function getDateRealisation(): ?\DateTimeInterface
    {
        return $this->dateRealisation;
    }

    /**
     * définit la date de réalisation de l'entité
     * @return self objet en cours
     */
    public function setDateRealisation(\DateTimeInterface $dateRealisation): self
    {
        $this->dateRealisation = $dateRealisation;

        return $this;
    }

    /**
     * recupère l'image de l'entité
     * @return string image de l'entité
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * définit l'image de l'entité
     * @return self objet en cours
     */
    public function setImage($image): self
    {
      if (is_string($image)) {
        $this->image = $image;
      }

        return $this;
    }

    /**
     * upload l'image sur le serveur
     * @param File $image objet image à upload
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateModification = new \DateTime();
        }
    }

    /**
     * recupère l'objet image
     * @return File objet image de l'entité
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * recupère la mise en avant de l'entité
     * @return bool mise en avant de l'entité
     */
    public function getMisEnAvant(): ?bool
    {
        return $this->misEnAvant;
    }

    /**
     * définit la mise en avant de l'entité
     * @return self objet en cours
     */
    public function setMisEnAvant(bool $misEnAvant): self
    {
        $this->misEnAvant = $misEnAvant;

        return $this;
    }

    /**
     * recupère le Createur de l'entité
     * @return Createur Createur de l'entité
     */
    public function getCreateur(): ?Createur
    {
        return $this->createur;
    }

    /**
     * définit le Createur de l'entité
     * @return self objet en cours
     */
    public function setCreateur(?Createur $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * recupère le Client de l'entité
     * @return Client Client de l'entité
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * définit le Client de l'entité
     * @return self objet en cours
     */
    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * récupère les Tag de l'entité
     * @return Collection|Tag[] Tag de l'entité
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * ajoute un Tag à l'entité
     * @param  Tag $tag Tag à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    /**
     * supprime un Tag à l'entité
     * @param  Tag $tag Tag à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * recupère la date de modification de l'entité
     * @return \DateTime date de modification
     */
    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    /**
     * définit la date de modification de l'entité
     * @return self objet en cours
     */
    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * utile pour que easyAdmin aie accès aux tables de traductions
     * @param  string $name nom de la propriété
     * @return mixed  renvoies le contenu de la méthode
     */
    public function __get($name)
    {
        $method = 'get'. ucfirst($name);
        $arguments = [];
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * permet de récuperer les méthodes de la table de traduction
     * @param  string $method nom de la méthode
     * @param  mixed $args   arguments éventuels
     * @return mixed         contenu de la méthode
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNom();
    }

}
