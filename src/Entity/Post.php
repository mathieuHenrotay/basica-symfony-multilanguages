<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @Vich\Uploadable
 */
class Post
{
    use ORMBehaviors\Translatable\Translatable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * date de publication de l'entité
     * @var \DateTime
     */
    private $datePublication;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="post_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categorie", inversedBy="posts")
     * ensemble des Categorie de l'entité
     * @var Categorie[]
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="post")
     * ensemble des Commentaire de l'entité
     * @var Commentaire[]
     */
    private $commentaires;

    /**
     * @ORM\Column(type="datetime")
     * date de modification de l'entité (utile pour l'envoi d'image)
     * @var \DateTime
     */
    private $dateModification;
    /**
     * contructeur d'objet
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * recupère la date de publication de l'entité
     * @return \DateTime date de publication
     */
    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->datePublication;
    }

    /**
     * définit la date de publication de l'entité
     * @return self objet en cours
     */
    public function setDatePublication(\DateTimeInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * recupère l'image de l'entité
     * @return string image de l'entité
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * définit l'image de l'entité
     * @return self objet en cours
     */
    public function setImage($image): self
    {
      if (is_string($image)) {
        $this->image = $image;
      }

      return $this;
    }

    /**
     * upload l'image sur le serveur
     * @param File $image objet image à upload
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateModification = new \DateTime();
        }
    }

    /**
     * recupère l'objet image
     * @return File objet image de l'entité
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * récupère les Categorie de l'entité
     * @return Collection|Categorie[] Categorie de l'entité
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * ajoute un Categorie à l'entité
     * @param  Categorie $category Categorie à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addCategory(Categorie $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * supprime une Categorie à l'entité
     * @param  Categorie $category Categorie à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removeCategory(Categorie $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    /**
     * récupère les Commentaire de l'entité
     * @return Collection|Commentaire[] Commentaire de l'entité
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    /**
     * ajoute un Commentaire à l'entité
     * @param  Commentaire $commentaire Commentaire à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setPost($this);
        }

        return $this;
    }

    /**
     * supprime un Commentaire à l'entité
     * @param  Commentaire $commentaire Commentaire à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getPost() === $this) {
                $commentaire->setPost(null);
            }
        }

        return $this;
    }

    /**
     * recupère la date de modification de l'entité
     * @return \DateTime date de modification
     */
    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    /**
     * définit la date de modification de l'entité
     * @return self objet en cours
     */
    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * utile pour que easyAdmin aie accès aux tables de traductions
     * @param  string $name nom de la propriété
     * @return mixed  renvoies le contenu de la méthode
     */
    public function __get($name)
    {
        $method = 'get'. ucfirst($name);
        $arguments = [];
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * permet de récuperer les méthodes de la table de traduction
     * @param  string $method nom de la méthode
     * @param  mixed $args   arguments éventuels
     * @return mixed         contenu de la méthode
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNom();
    }

}
