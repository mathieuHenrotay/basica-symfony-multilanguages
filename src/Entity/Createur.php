<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreateurRepository")
 */
class Createur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * nom de l'entité
     * @var string
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Work", mappedBy="createur")
     * ensemble des Work de l'entité
     * @var Work[]
     */
    private $works;

    /**
     * contruct de l'objet
     */
    public function __construct()
    {
        $this->works = new ArrayCollection();
    }

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * recupère le nom de l'entité
     * @return string nom de l'entité
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * définit le nom de l'entité
     * @return self objet en cours
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * récupère les Work de l'entité
     * @return Collection|Work[] Work de l'entité
     */
    public function getWorks(): Collection
    {
        return $this->works;
    }

    /**
     * ajoute un Work à l'entité
     * @param  Work $work Work à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addWork(Work $work): self
    {
        if (!$this->works->contains($work)) {
            $this->works[] = $work;
            $work->setCreateur($this);
        }

        return $this;
    }

    /**
     * supprime un Work à l'entité
     * @param  Work $work Work à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removeWork(Work $work): self
    {
        if ($this->works->contains($work)) {
            $this->works->removeElement($work);
            // set the owning side to null (unless already changed)
            if ($work->getCreateur() === $this) {
                $work->setCreateur(null);
            }
        }

        return $this;
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNom();
    }
}
