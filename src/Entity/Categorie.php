<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    use ORMBehaviors\Translatable\Translatable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Post", mappedBy="categories")
     * ensemble des Post de l'entité
     * @var Post[]
     */
    private $posts;

    /**
     * contruct de l'objet
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * récupère les posts de l'entité
     * @return Collection|Post[] Post de l'entité
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * ajoute un post à l'entité
     * @param  Post $post Post à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->addCategory($this);
        }

        return $this;
    }

    /**
     * supprime un Post à l'entité
     * @param  Post $post Post à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            $post->removeCategory($this);
        }

        return $this;
    }

    /**
     * utile pour que easyAdmin aie accès aux tables de traductions
     * @param  string $name nom de la propriété
     * @return mixed  renvoies le contenu de la méthode
     */
    public function __get($name)
    {
        $method = 'get'. ucfirst($name);
        $arguments = [];
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * permet de récuperer les méthodes de la table de traduction
     * @param  string $method nom de la méthode
     * @param  mixed $args   arguments éventuels
     * @return mixed         contenu de la méthode
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNom();
    }
}
