<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    use ORMBehaviors\Translatable\Translatable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * ordre de l'entité
     * @var int
     */
    private $tri;

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * recupère l'ordre de l'entité
     * @return int ordre de l'entité
     */
    public function getTri(): ?int
    {
        return $this->tri;
    }

    /**
     * définit l'ordre de l'entité
     * @return self objent en cours
     */
    public function setTri(int $tri): self
    {
        $this->tri = $tri;

        return $this;
    }

    /**
     * utile pour que easyAdmin aie accès aux tables de traductions
     * @param  string $name nom de la propriété
     * @return mixed  renvoies le contenu de la méthode
     */
    public function __get($name)
    {
        $method = 'get'. ucfirst($name);
        $arguments = [];
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * permet de récuperer les méthodes de la table de traduction
     * @param  string $method nom de la méthode
     * @param  mixed $args   arguments éventuels
     * @return mixed         contenu de la méthode
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNomMenu();
    }

}
