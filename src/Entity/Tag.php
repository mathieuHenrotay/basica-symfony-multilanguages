<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    use ORMBehaviors\Translatable\Translatable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de l'entité
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Work", mappedBy="tags")
     * @ORM\OrderBy({"dateRealisation" = "DESC"})
     * ensemble des Work de l'entité
     * @var Work[]
     */
    private $works;

    /**
     * contruct de l'objet
     */
    public function __construct()
    {
        $this->works = new ArrayCollection();
    }

    /**
     * recupère l'id de l'entité
     * @return int id de l'entité
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * récupère les Work de l'entité
     * @return Collection|Work[] Work de l'entité
     */
    public function getWorks(): Collection
    {
        return $this->works;
    }

    /**
     * ajoute un Work à l'entité
     * @param  Work $work Work à ajouter
     * @return self       renvoit l'objet en cours
     */
    public function addWork(Work $work): self
    {
        if (!$this->works->contains($work)) {
            $this->works[] = $work;
            $work->addTag($this);
        }

        return $this;
    }

    /**
     * supprime un Work à l'entité
     * @param  Work $work Work à supprimer
     * @return self       renvoies l'objet en cours
     */
    public function removeWork(Work $work): self
    {
        if ($this->works->contains($work)) {
            $this->works->removeElement($work);
            $work->removeTag($this);
        }

        return $this;
    }

    /**
     * utile pour que easyAdmin aie accès aux tables de traductions
     * @param  string $name nom de la propriété
     * @return mixed  renvoies le contenu de la méthode
     */
    public function __get($name)
    {
        $method = 'get'. ucfirst($name);
        $arguments = [];
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * permet de récuperer les méthodes de la table de traduction
     * @param  string $method nom de la méthode
     * @param  mixed $args   arguments éventuels
     * @return mixed         contenu de la méthode
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }

    /**
     * convertit un objet en string (définit le champ utilisé pour la conversion en question)
     * @return string contenu du champ qui représente l'entité (ici le champ nom)
     */
    public function __toString()
    {
      return $this->getNom();
    }

}
