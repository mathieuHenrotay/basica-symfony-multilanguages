-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2019 at 11:09 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `basica-symfony-multilingue`
--
CREATE DATABASE IF NOT EXISTS `basica-symfony-multilingue` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `basica-symfony-multilingue`;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `categorie_translation`
--

CREATE TABLE `categorie_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie_translation`
--

INSERT INTO `categorie_translation` (`id`, `translatable_id`, `nom`, `slug`, `locale`) VALUES
(1, 1, 'Development', 'development', 'en'),
(2, 1, 'Développement', 'developpement', 'fr'),
(3, 2, 'Design', 'Design', 'en'),
(4, 2, 'Design', 'Design', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `nom`) VALUES
(1, 'Microsoft'),
(2, 'Apple'),
(3, 'Android');

-- --------------------------------------------------------

--
-- Table structure for table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_publication` datetime NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `createur`
--

CREATE TABLE `createur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `createur`
--

INSERT INTO `createur` (`id`, `nom`) VALUES
(1, 'Mathieu'),
(2, 'Philippe');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'admin', 'admin', 'mathieu.henrotay@gmail.com', 'mathieu.henrotay@gmail.com', 1, NULL, '$2y$13$zujju9Y3SBz0n3ysWH87VOqM8yOFo5AztLjnqcXIk/xhaSHtQi7gW', '2019-04-04 00:24:32', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}');

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190404001850', '2019-04-04 00:21:10');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `tri` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `tri`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `page_translation`
--

CREATE TABLE `page_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_translation`
--

INSERT INTO `page_translation` (`id`, `translatable_id`, `nom_menu`, `nom`, `slug`, `texte`, `locale`) VALUES
(1, 1, 'Home', 'home', 'home', NULL, 'en'),
(2, 1, 'Accueil', 'Accueil', 'accueil', NULL, 'fr'),
(3, 2, 'Portfolio', 'Our portfolio', 'portfolio', '<h2>We are leading company</h2><h3>Specializing in Wordpress Theme Development</h3><p>Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis.</p>', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `date_publication` datetime NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `date_publication`, `photo`) VALUES
(1, '2019-01-24 00:00:00', '1.jpg'),
(2, '2019-01-02 00:00:00', '10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `post_categorie`
--

CREATE TABLE `post_categorie` (
  `post_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_categorie`
--

INSERT INTO `post_categorie` (`post_id`, `categorie_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `post_translation`
--

CREATE TABLE `post_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_translation`
--

INSERT INTO `post_translation` (`id`, `translatable_id`, `nom`, `slug`, `texte`, `locale`) VALUES
(1, 1, 'article about dev', 'article-about-dev', 'wonderfull text about dev', 'en'),
(2, 1, 'article sur le développement', 'article-sur-le-developpement', 'du beau texte bien écrit', 'fr'),
(3, 2, 'post about design', 'post-about-design', 'english Vivamus interdum sem orci, vel aliquet nibh viverra sit amet. Integer eleifend ipsum in ante ornare luctus. Integer nec tempus arcu, nec dictum diam. Curabitur mollis ligula quis rutrum auctor. Pellentesque hendrerit mollis egestas. Etiam sit amet pharetra tellus. Phasellus fringilla finibus leo, non viverra justo. Donec lorem ligula, fermentum sed ipsum sit amet, vehicula pellentesque arcu. Etiam et pellentesque leo. Ut nec neque vitae magna consequat egestas a eget ipsum.', 'en'),
(4, 2, 'Un article de design', 'un-article-de-design', 'salut a tous', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`) VALUES
(1),
(2),
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tag_translation`
--

CREATE TABLE `tag_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_translation`
--

INSERT INTO `tag_translation` (`id`, `translatable_id`, `nom`, `slug`, `locale`) VALUES
(1, 1, 'dev', 'dev', 'en'),
(2, 1, 'dev', 'dev', 'fr'),
(3, 2, 'design', 'design', 'en'),
(4, 2, 'design', 'design', 'fr'),
(5, 3, 'Symfony', 'symfony', 'en'),
(6, 3, 'Symfony', 'symfony', 'fr');

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `createur_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_realisation` datetime NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mis_en_avant` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`id`, `createur_id`, `client_id`, `date_realisation`, `photo`, `mis_en_avant`) VALUES
(1, 1, 2, '2019-01-01 00:00:00', '1.jpg', 1),
(2, 2, 3, '2019-01-24 00:00:00', '2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_tag`
--

CREATE TABLE `work_tag` (
  `work_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_tag`
--

INSERT INTO `work_tag` (`work_id`, `tag_id`) VALUES
(1, 1),
(1, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `work_translation`
--

CREATE TABLE `work_translation` (
  `id` int(11) NOT NULL,
  `translatable_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte_court` longtext COLLATE utf8mb4_unicode_ci,
  `texte_long` longtext COLLATE utf8mb4_unicode_ci,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_translation`
--

INSERT INTO `work_translation` (`id`, `translatable_id`, `nom`, `slug`, `texte_court`, `texte_long`, `locale`) VALUES
(1, 1, 'IOS 13', 'ios-13', 'A short description of the project', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor pellentesque. Mauris maximus tortor sed dolor tristique finibus. Donec ac egestas mi. Sed lorem metus, tempor eu erat sed, viverra tincidunt urna. Fusce ornare nibh libero, sit amet tincidunt felis egestas et. Curabitur sit amet fringilla est. In lacinia lacinia augue vel porttitor. Vivamus laoreet eu arcu sit amet consequat. Quisque consequat urna viverra pulvinar ornare. Vestibulum ac purus pulvinar dui ullamcorper tempus. Cras at dolor dui. Proin vestibulum neque vel ornare tristique.', 'en'),
(2, 1, 'IOS 13', 'ios-13', 'Une brève description de IOS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor pellentesque. Mauris maximus tortor sed dolor tristique finibus. Donec ac egestas mi. Sed lorem metus, tempor eu erat sed, viverra tincidunt urna. Fusce ornare nibh libero, sit amet tincidunt felis egestas et. Curabitur sit amet fringilla est. In lacinia lacinia augue vel porttitor. Vivamus laoreet eu arcu sit amet consequat. Quisque consequat urna viverra pulvinar ornare. Vestibulum ac purus pulvinar dui ullamcorper tempus. Cras at dolor dui. Proin vestibulum neque vel ornare tristique.', 'fr'),
(3, 2, 'english lorem ipsum sic amet', 'english-lorem-ipsum-sic-amet', 'english Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor pellentesque. Mauris maximus tortor sed dolor', 'english Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor pellentesque. Mauris maximus tortor sed dolor tristique finibus. Donec ac egestas mi. Sed lorem metus, tempor eu erat sed, viverra tincidunt urna. Fusce ornare nibh libero, sit amet tincidunt felis egestas et. Curabitur sit amet fringilla est. In lacinia lacinia augue vel porttitor. Vivamus laoreet eu arcu sit amet consequat. Quisque consequat urna viverra pulvinar ornare. Vestibulum ac purus pulvinar dui ullamcorper tempus. Cras at dolor dui. Proin vestibulum neque vel ornare tristique.', 'en'),
(4, 2, 'lorem ipsum sic amet', 'lorem-ipsum-sic-amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue odio, imperdiet in venenatis eget, sagittis ac magna. Fusce in risus mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque sit amet dolor quis ante porttitor pellentesque. Mauris maximus tortor sed dolor tristique finibus. Donec ac egestas mi. Sed lorem metus, tempor eu erat sed, viverra tincidunt urna. Fusce ornare nibh libero, sit amet tincidunt felis egestas et. Curabitur sit amet fringilla est. In lacinia lacinia augue vel porttitor. Vivamus laoreet eu arcu sit amet consequat. Quisque consequat urna viverra pulvinar ornare. Vestibulum ac purus pulvinar dui ullamcorper tempus. Cras at dolor dui. Proin vestibulum neque vel ornare tristique.', 'fr');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorie_translation_unique_translation` (`translatable_id`,`locale`(10)),
  ADD KEY `IDX_6B9763662C2AC5D3` (`translatable_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_67F068BC4B89032C` (`post_id`),
  ADD KEY `IDX_67F068BCA76ED395` (`user_id`);

--
-- Indexes for table `createur`
--
ALTER TABLE `createur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translation`
--
ALTER TABLE `page_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translation_unique_translation` (`translatable_id`,`locale`(10)),
  ADD KEY `IDX_A3D51B1D2C2AC5D3` (`translatable_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_categorie`
--
ALTER TABLE `post_categorie`
  ADD PRIMARY KEY (`post_id`,`categorie_id`),
  ADD KEY `IDX_E813A8C34B89032C` (`post_id`),
  ADD KEY `IDX_E813A8C3BCF5E72D` (`categorie_id`);

--
-- Indexes for table `post_translation`
--
ALTER TABLE `post_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_translation_unique_translation` (`translatable_id`,`locale`(10)),
  ADD KEY `IDX_5829CF402C2AC5D3` (`translatable_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_translation`
--
ALTER TABLE `tag_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_translation_unique_translation` (`translatable_id`,`locale`(10)),
  ADD KEY `IDX_A8A03F8F2C2AC5D3` (`translatable_id`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_534E688073A201E5` (`createur_id`),
  ADD KEY `IDX_534E688019EB6921` (`client_id`);

--
-- Indexes for table `work_tag`
--
ALTER TABLE `work_tag`
  ADD PRIMARY KEY (`work_id`,`tag_id`),
  ADD KEY `IDX_79E7E01FBB3453DB` (`work_id`),
  ADD KEY `IDX_79E7E01FBAD26311` (`tag_id`);

--
-- Indexes for table `work_translation`
--
ALTER TABLE `work_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `work_translation_unique_translation` (`translatable_id`,`locale`(10)),
  ADD KEY `IDX_ABA8A5AC2C2AC5D3` (`translatable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `createur`
--
ALTER TABLE `createur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `page_translation`
--
ALTER TABLE `page_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `post_translation`
--
ALTER TABLE `post_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tag_translation`
--
ALTER TABLE `tag_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `work_translation`
--
ALTER TABLE `work_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categorie_translation`
--
ALTER TABLE `categorie_translation`
  ADD CONSTRAINT `FK_6B9763662C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `FK_67F068BC4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_67F068BCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `page_translation`
--
ALTER TABLE `page_translation`
  ADD CONSTRAINT `FK_A3D51B1D2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_categorie`
--
ALTER TABLE `post_categorie`
  ADD CONSTRAINT `FK_E813A8C34B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E813A8C3BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_translation`
--
ALTER TABLE `post_translation`
  ADD CONSTRAINT `FK_5829CF402C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `post` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tag_translation`
--
ALTER TABLE `tag_translation`
  ADD CONSTRAINT `FK_A8A03F8F2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `FK_534E688019EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FK_534E688073A201E5` FOREIGN KEY (`createur_id`) REFERENCES `createur` (`id`);

--
-- Constraints for table `work_tag`
--
ALTER TABLE `work_tag`
  ADD CONSTRAINT `FK_79E7E01FBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_79E7E01FBB3453DB` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work_translation`
--
ALTER TABLE `work_translation`
  ADD CONSTRAINT `FK_ABA8A5AC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `work` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
